package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.UsuarioDAO;
import root.model.entities.Usuario;



@WebServlet(name = "controllerUsuario", urlPatterns = {"/controllerUsuario"})
public class ControllerUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

            System.out.println("controllerUsuario ")  ; 
            
            String username = request.getParameter("username");
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String sexo = request.getParameter("sexo");
            String accion = request.getParameter("accion");
            
            System.out.println("username "+username)  ;
            System.out.println("nombre "+nombre)  ;
            System.out.println("apellido "+apellido);
            System.out.println("sexo "+sexo);
            
        if (accion.equalsIgnoreCase("grabarCrear")) {
            try {
 
            UsuarioDAO dao=new UsuarioDAO();
            
            Usuario us=new Usuario();
            us.setUsername(username);
            us.setNombre(nombre);
            us.setApellido(apellido);
            us.setSexo(sexo);
            dao.create(us);
            
            List<Usuario> usuarios=dao.findUsuarioEntities();
            System.out.println("Cantidad clientes en BD  "+usuarios.size())  ;
            
            request.setAttribute("usuarios", usuarios);
            request.getRequestDispatcher("lista.jsp").forward(request,response);
        } catch (Exception ex) {
            Logger.getLogger(ControllerUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
         
        if (accion.equalsIgnoreCase("grabarEditar")) {
        try {
 
            UsuarioDAO dao=new UsuarioDAO();
            
        
            Usuario us=new Usuario();
            us.setUsername(username);
            us.setNombre(nombre);
            us.setApellido(apellido);
            us.setSexo(sexo);
            
            dao.edit(us);
            
            List<Usuario> usuarios=dao.findUsuarioEntities();
            System.out.println("Cantidad clientes en BD  "+usuarios.size())  ;
            
            request.setAttribute("usuarios", usuarios);
            request.getRequestDispatcher("lista.jsp").forward(request,response);
        } catch (Exception ex) {
            Logger.getLogger(ControllerUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        } 
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
