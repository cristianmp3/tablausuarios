
package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import root.model.entities.Usuario;

@Path("/usuarios")


public class UsuariosRest {
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    EntityManager em;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
  
    public Response listarTodo(){
        em= emf.createEntityManager();
        List<Usuario> lista =em.createNamedQuery("Usuario.findAll").getResultList();
        return Response.ok(200).entity(lista).build();
    }
    
    @GET 
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response buscarID(@PathParam("idbuscar")String idbuscar){
    
        em= emf.createEntityManager();
        Usuario usuario =em.find(Usuario.class, idbuscar);
        return Response.ok(200).entity(usuario).build();
    
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String nuevo(Usuario usuario){
        em= emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(usuario);
        em.getTransaction().commit();
        
     return "Usuario Guardado";
    
}
    
    @PUT
    public Response actualizar(Usuario usuario){
        em= emf.createEntityManager();
        em.getTransaction().begin();
        usuario= em.merge(usuario);
        em.persist(usuario);
        em.getTransaction().commit();
    return Response.ok(200).entity(usuario).build();
       
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
      public Response eliminaId(@PathParam("iddelete") String iddelete){
      em= emf.createEntityManager();
      em.getTransaction().begin();
      Usuario usuario= em.getReference(Usuario.class, iddelete);
      em.remove(usuario);
      em.getTransaction().commit();
      return Response.ok("cliente eliminado").build();
    
    
}
      
}
